# **WikiRacing**
WikiRacing is a Python script that finds the shortest path between two Wikipedia articles using breadth-first search algorithm.

## **Requirements**
- Python 3.x
- PostgreSQL 15.x
- pgAdmin 4 (optional)

## **Installation**
1. Clone the repository:
    ```bush
    git clone https://github.com/<username>/wikiracing.git
    cd wikiracing
    ```
2. Install dependencies:
    ```
    pip install -r requirements.txt
    ```
3. Set up environment variables:

    Create a .env file in the root directory with the following variables:
    ```
    HOST=postgres
    DB_NAME=<db_name>
    USER=<db_username>
    PASSWORD=<db_password>
    PORT=5432
    ```
4. Start the PostgreSQL and pgAdmin containers:
    ```
    docker-compose up -d
    ```
## **Usage**
Run the `wikiracing.py` script with the following command:
```
python wikiracing.py
```
By default, the script finds the shortest path between "Ukraine" and "Rurik dynasty" articles on English Wikipedia. You can change the starting and ending articles by modifying the arguments passed to the `find_path()` method in the `if __name__ == "__main__"`: block.
