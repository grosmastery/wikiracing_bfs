from typing import List, Tuple, Any
from html.parser import HTMLParser
import requests
from urllib.parse import unquote
from queue import Queue
import psycopg2
from ratelimiter import RateLimiter
import os

requests_per_minute = 100
links_per_page = 200
MINUTE = 60


class DBConnection:
    def __init__(self):
        self.conn = psycopg2.connect(host=os.environ.get("HOST"), database=os.environ.get("DB_NAME"),
                                     user=os.environ.get("USER"), password=os.environ.get("PASSWORD"),
                                     port=os.environ.get("PORT"))
        self.cur = self.conn.cursor()
        self.cur.execute("create table if not exists page (id serial primary key,name varchar(300) unique not null);")
        self.cur.execute(
            "create table if not exists link(from_page int not null,to_page int not null, primary key(from_page,to_page),"
            " foreign key(from_page) references page(id),foreign key(to_page) references page(id));")

    def add_page(self, page: str) -> int:
        self.cur.execute("INSERT INTO page (name) VALUES (%s) ON CONFLICT (name) DO NOTHING RETURNING id", (page,))
        self.conn.commit()
        return self.cur.fetchone()[0]

    def get_page_id(self, page: str) -> int:
        self.cur.execute("SELECT id FROM page WHERE name = %s", (page,))
        id = self.cur.fetchone()
        if id:
            return id[0]
        else:
            return self.add_page(page)

    def get_page_name(self, id: int) -> str:
        self.cur.execute("SELECT name FROM page WHERE id = %s", (id,))
        return self.cur.fetchone()[0]

    def get_page_links(self, page: str) -> list[tuple[Any, ...]]:
        self.cur.execute("""
            SELECT page.id, page.name
            FROM link 
            JOIN page ON link.to_page = page.id
            WHERE link.from_page = (SELECT id FROM page WHERE name = %s)""", (page,))
        return self.cur.fetchall()

    def add_links(self, from_page: int, to_pages: List[int]):
        values = ', '.join(f"({from_page}, {to_page})" for to_page in to_pages)
        self.cur.execute(f"""
            INSERT INTO link (from_page, to_page)
            VALUES {values}
            ON CONFLICT DO NOTHING""")
        self.conn.commit()

    def __del__(self):
        self.conn.close()


class MyHTMLParser(HTMLParser):
    links = []

    def handle_starttag(self, tag, attrs):
        attrs = dict(attrs)
        if tag == "a" and "href" in attrs and attrs['href'].startswith('/wiki/') and ":" not in attrs['href']:
            if len(self.links) < links_per_page:
                self.links.append(attrs['href'])

    def get_links(self) -> List[str]:
        temp = self.links
        self.links = []
        for i in range(len(temp)):
            temp[i] = unquote(temp[i][6:]).replace("_", " ")
        return temp


@RateLimiter(max_calls=requests_per_minute, period=MINUTE)
def get(url: str) -> str:
    while True:
        try:
            return requests.get(url).text
        except requests.exceptions.HTTPError as e:
            print(e.response.text)


class WikiRacer:
    def __init__(self):
        self.parser = MyHTMLParser()
        self.db = DBConnection()

    def find_path(self, start: str, finish: str) -> List[str]:
        q = Queue()
        q.put([start])
        visited = set()

        while not q.empty():
            path = q.get()
            page = path[-1]

            if page in visited:
                continue

            id = self.db.get_page_id(page)
            links = self.db.get_page_links(page)

            if not bool(links):
                self.parser.feed(get("https://en.wikipedia.org/wiki/" + page))
                links = self.parser.get_links()
                links = [(self.db.get_page_id(i), i) for i in links]
                self.db.add_links(id, [i[0] for i in links])

            for i in links:
                new_path = path + [i[1]]
                if i[1] == finish:
                    return new_path
                q.put(new_path)

            visited.add(page)
            print(*path)


if __name__ == "__main__":
    path = WikiRacer().find_path('Ukraine', 'Rurik dynasty')
    print(path)


